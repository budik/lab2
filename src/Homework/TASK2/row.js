import React from 'react';

const Row = ({head,children}) => {
console.log({head});
let {header} = head;


if(head) {
    return <thead>
    <tr>
        <th>{children}</th>
    </tr>
    </thead>;
}
else {
    return <tbody>
    <tr>
        {children}
    </tr>
    </tbody>;
}


}


Row.defaultProps= {

    head:false,
    children:<tbody><tr><td>No data</td></tr></tbody>

}

export default Row;

