import React from 'react';


const Cell  = ({children,type,cells,background,color,currency}) => {
    function type_selected(type)  {
         switch(type) {

            case "DATE":   return   {fontStyle:'italic'};

            case "NUMBER":   return    {align:'right'};
            case "MONEY": return    {align:'right'};
            case "TEXT":  return    {align:'right'};

            default:      return   {align:'right'};
        }
    }
    const styled = type_selected(type);
    if(background){
        styled['background'] = background;
    }
    if(color){
        styled['color'] = color;
    }

    const money = (currency) => {
        if (type=="MONEY"){
            if(currency){
            return(
                currency= currency
            )}
            else{
                return(
                    console.log('laga ne peredan param valuti')

                )}
            }

        }

  const curr = money(currency);

    console.log('this is  styled ',styled , typeof(styled));

    return(

        <td colSpan={cells}
            style={styled}
            background={background}
            color={color}

        >{children} {curr} </td>

    )
}

Cell.defaultProps = {
    type: 'text',
        cells: 1,
    background: 'transparent',
        color: 'black'
}

export default Cell;