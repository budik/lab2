import React,{Component} from 'react';

class LoaderImg extends Component{


    constructor(props) {
        super(props);

        console.log('Lifecycle 0 Constructor ');
        this.state = {
            loaded:false,
            query:'',
            data:[], date:new Date()
        }

    }
    //
    // static getDerivedStateFromProps( props,state){
    //
    //     console.log('Lifecycle 1 getDerivedStateFromProps ',props,state);
    //     if(props.id === 1){
    //         return {
    //             loaded:true,
    //             data:[1,2,3],
    //             query:'Not CHANGE'
    //             // , date:new Date()
    //
    //         }
    //     }
    //
    //     return null;
    // }

    // changeHamdler = (e) => {
    //     this.setState(
    //         {
    //             query:e.target.value
    //         }
    //     )
    // }


    componentDidMount = () => {
        console.log('Lifecycle 3 componentDidMount ');
        fetch('https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png" ')
            .then(response => this.setState({
                data:response,
                loaded:true
            }));

       /* this.timer = setInterval(()=>{
            let date = new Date();
            // console.log(date,' date was');

        this.setState({
            date
        })
        },1000);*/

    }

shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('LifeCycle 4  Should Component update');

    console.log(this.props
        ,' this is current props '
        ,this.state
        ,' this is current state '
        ,this.context
        ,' this is current context '
    );


    console.log(nextProps
            ,' this is next props '
            ,nextState
            ,' this is next state '
            ,nextContext
        ,' this is next context '
        );

    if (nextProps.id === '1'){
        return false;
    }

        return true;

}


componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('Lifecycle 6 Shoul component update');
        console.log(prevProps
            ,' this is prev props '
            ,prevState
            ,' this is prev state '
            ,snapshot
            ,' this is snapshot'
        );
}

    render() {

        console.log('Lifecycle 2 RENDER ');
        const {query,data,date} = this.state;

        // if(data.length ===0){
        //     return (
        //         <h1>LOADING</h1>
        //     )
        //
        // }


        return (
            <div>

                    <h1>Lifecycle {Math.random()}</h1>
                <img src={data}></img>
                {/*<input value={query} onChange={this.changeHamdler}/>*/}
                {/*<button onClick={this.forceUpdateHandler}>Force update</button>*/}
                {/*<h3>{ date.toLocaleTimeString() }</h3>*/}
{/*                <ul>{*/}
{/*                    data.map( item => {*/}
{/*                 return (*/}
{/*                     <li key={item.id}>*/}
{/*                    {item.title}*/}
{/*                    </li>*/}
{/*                    )*/}
{/*                })*/}
{/*}*/}
{/*                </ul>*/}

            </div>

        )
    }

}

export default LoaderImg;


