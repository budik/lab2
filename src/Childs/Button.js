import React from 'react';


const Button = ({action,style,children}) => {


    return(
        <button
        onClick={action}
        style={style}
        >{children}</button>
    )
}
export default Button;