import React,{Component} from 'react';
import Button from "./Button";
import Message from "../Components/functional";
import ButtonClasswork from "../Classwork/ButtonClasswork";

const Parent = () =>{
    return (
        <div>
            <h1 style={{color:"red"}}> Parent</h1>
<Button
style={{backgroundColor:"#ffcc00",padding:"5px 20px"}}
action={() => {console.log('HEllo')}}
>TExt</Button>
<ButtonClasswork
style={{backgroundColor:"",padding:"10px 40px"}}
action={() => {console.log('HEllo')}}
/>
            <ButtonClasswork>

    {/*React Compo*/}
    {/*<Message/>*/}

    {/*TExt*/}
    {/*Some TExt*/}

    {/*HTML*/}
    {/*<img width="20" src="https://www.flaticon.com/svg/static/icons/svg/70/70886.svg"/>*/}

    {/*Boolean*/}
    {/*{false}*/}

    {/*Null*/}
    {/*{null}*/}

    {/*Undefined*/}
    {/*{undefined}*/}

    {/*expression*/}
    {/*{10+30}*/}

      </ButtonClasswork>
        </div>

    )

}
export default Parent;