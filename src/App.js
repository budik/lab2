import React,{Component} from 'react';
import logo from './logo.svg';
// import './App.css';
import Message from "./Components/functional";
import SomeThing from "./Components/classed";
import BindingDemo from "./Binding/binding";
import Parent from "./Childs";
import Lifecycle from "./Lifecycles/LifecycleDemo";
import Task2 from "./Classwork/TASK2";
import LoaderImg from "./Homework/Preloader_task1";
import Table from "./Homework/TASK2/table";
import Row from "./Homework/TASK2/row";
import Cell from "./Homework/TASK2/cell";


class App extends Component{

  state = {
    show:true
  }

  changeStatus = () => {

    this.setState({
      show: !this.state.show
    });

  }


  render = () => {

    const {show} = this.state;
    const head = true;

    return (
        <div className="App">
          {/*<button onClick={this.changeStatus}>
            {show ? "Hide" : "Show"}

          </button>*/}

          {/* <Message
          element={{
              title:"Hello world title from mess props component",
              message:"It basically message"
          }}
      >
      </Message>
<Message>
</Message>
        <SomeThing
        title="Class comp title"
        ></SomeThing>
*/}
          {/*<BindingDemo></BindingDemo>*/}

          {/*{ show &&
          (<Lifecycle id="2"/>)
          }*/}


          {/*<Parent/>*/}
          {/*<Classwork/>*/}


          {/*<Task2></Task2>*/}


          {/*HOMEWORK*/}
          {/*<LoaderImg/>*/}
          <Table><Row head={head}>crazy</Row>
            <Row >
              <Cell cells="3" color="red" type="NUMBER">1</Cell>
              <Cell cells="2"
                  type="DATE">2</Cell>
              <Cell cells="1" type="MONEY" currency="$">3</Cell>
              <Cell cells="1" background="powderblue">4</Cell>
            </Row>
            </Table>

          </div>
    )
  }

}
/*
function App() {
  return (

  );
}*/

export default App;
