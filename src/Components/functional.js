import React from 'react';

const Message = ({element}) =>{
    // console.log(props);
    const {title,message} = element;
    return(
<div>
    <h2>{title}</h2>
    <p>{message}</p>
</div>

    );
}

Message.defaultProps = {
    element:{
        title:"default title",
        message:"default mes"
    }
}

export default Message;