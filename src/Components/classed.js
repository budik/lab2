import  React,{Component} from 'react';
import Message from "./functional";


class SomeThing extends Component{

    state = {
    messages: [
        {title:"Dogo",message:"Woof"},
        {title:"Cat",message:"Meo"},
{title:"Cow",message:"Moo"}
            ]
        ,counter:0
    }

    changer = () => {
        const {messages,counter} = this.state;

        let newIndex = counter + 1;
        if (newIndex > messages.length - 1){
            newIndex = 0;
        }

        this.setState({
            counter:newIndex
        })
    }

    render = () => {
        const {title} = this.props;
        const {messages,counter} = this.state;
        const {changer} = this;
        return(
            <div>
                <h1>{title}</h1>
                <button onClick={changer}>Change!</button>
                <Message
                element={messages[counter]}
                ></Message>
            </div>
        )


    }

}



export default SomeThing;