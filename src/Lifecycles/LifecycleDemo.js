import React,{Component} from 'react';

class Lifecycle extends Component{


    constructor(props) {
        super(props);

        console.log('Lifecycle 0 Constructor ');
        this.state = {
            loaded:false,
            query:'',
            data:[], date:new Date()
        }

    }

    static getDerivedStateFromProps( props,state){

        console.log('Lifecycle 1 getDerivedStateFromProps ',props,state);
        if(props.id === 1){
            return {
                loaded:true,
                data:[1,2,3],
                query:'Not CHANGE'
                // , date:new Date()

            }
        }

        return null;
    }

    changeHamdler = (e) => {
        this.setState(
            {
                query:e.target.value
            }
        )
    }


    componentDidMount = () => {
        console.log('Lifecycle 3 componentDidMount ');
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(json => this.setState({
                data:json,
                loaded:true
            }));

       /* this.timer = setInterval(()=>{
            let date = new Date();
            // console.log(date,' date was');

        this.setState({
            date
        })
        },1000);*/

    }

shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('LifeCycle 4  Should Component update');

    console.log(this.props
        ,' this is current props '
        ,this.state
        ,' this is current state '
        ,this.context
        ,' this is current context '
    );


    console.log(nextProps
            ,' this is next props '
            ,nextState
            ,' this is next state '
            ,nextContext
        ,' this is next context '
        );

    if (nextProps.id === '1'){
        return false;
    }

        return true;

}

getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('LifeCycle 5  Should Component update');
        console.log(prevProps
        ,' this is prev props '
        ,prevState
        ,' this is prev state '
    );
    return 42;
}

componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('Lifecycle 6 Shoul component update');
        console.log(prevProps
            ,' this is prev props '
            ,prevState
            ,' this is prev state '
            ,snapshot
            ,' this is snapshot'
        );
}

componentWillUnmount = () => {
console.log('Lifecycle 7 Comp will unmount');
clearInterval(this.timer);
}

forceUpdateHandler = () => {
    this.forceUpdate();
}

    render() {

        console.log('Lifecycle 2 RENDER ');
        const {query,data,date} = this.state;

        if(data.length ===0){
            return (
                <h1>LOADING</h1>
            )

        }


        return (
            <div>

                    <h1>Lifecycle {Math.random()}</h1>
                <input value={query} onChange={this.changeHamdler}/>
                <button onClick={this.forceUpdateHandler}>Force update</button>
                {/*<h3>{ date.toLocaleTimeString() }</h3>*/}
                <ul>{
                    data.map( item => {
                 return (
                     <li key={item.id}>
                    {item.title}
                    </li>
                    )
                })
}
                </ul>

            </div>

        )
    }

}

export default Lifecycle;


