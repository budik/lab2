import React,{Component} from 'react';
import Task2ListComponent from "./task2ListComponent";

class Task2 extends Component{


    constructor(props) {
        super(props);

        console.log('Lifecycle 0 Constructor ');
        this.state = {
            loaded:false,
            query:'',
            data:[], date:new Date(),
            user: props.user

        }

    }


    changeHamdler = (e) => {
        this.setState(
            {
                query:e.target.value
            }
        )
    }


    componentDidMount = () => {
        console.log('Lifecycle 3 componentDidMount ');
        fetch('http://www.json-generator.com/api/json/get/cdZfJCjIia?indent=2')
            .then(response => response.json())
            .then(json => this.setState({
                data:json,
                loaded:true
            }));

        // console.log('wht data', this.data)

       /* this.timer = setInterval(()=>{
            let date = new Date();
            // console.log(date,' date was');

        this.setState({
            date
        })
        },1000);*/

    }


    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('LifeCycle 4  Should Component update');

        console.log(this.props
            ,' this is current props '
            ,this.state
            ,' this is current state '
            ,this.context
            ,' this is current context '
        );


        console.log(nextProps
            ,' this is next props '
            ,nextState
            ,' this is next state '
            ,nextContext
            ,' this is next context '
        );

        if (nextProps.id === '1'){
            return false;
        }

        return true;

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(prevProps
            ,' this is prevprops '
            ,prevState
            ,' thtis is prevstate ')
        return true;
    }
    // forceUpdateHandler = () => {
    //     this.forceUpdate();
    //     // console.log('force update log') ;
    //
    // }


    changeInterivewed = () => {

    }

    render() {

        console.log('Lifecycle 2 RENDER what about data',this.state.data);
        console.log('Lifecycle 2 RENDER what about props',this.props);
        const {query,data,date} = this.state;

        if(data.length ===0){
            return (
                <h1>LOADING</h1>
            )

        }


        return (
            <div>
{/*<p>{console.log(data,'/!**!/data inside jsx')}</p>*/}
                    <h1>Lifecycle {Math.random()}</h1>
                {/*<input value={query} onChange={this.changeHamdler}/>*/}
                {/*<button onClick={this.forceUpdateHandler}>Force update</button>*/}
                {/*<h3>{ date.toLocaleTimeString() }</h3>*/}
                <ul>{
                    data.map( item => {
                 return (
                     /*<li key={item.index}>
                         <p>{item.interviewed}</p>
                         <p>{item.user.name}</p>

                    </li>*/
                     <Task2ListComponent
                         key ={item.index}
                         user = {{ name:item.user.name,
                         gender:item.user.gender,
                         age:item.user.age,

                             interviewed: item.interviewed,
                         }}
                         action={() => {
                             this.forceUpdate();
                             console.log('wow it was passed from TASK2',item);
                             data[item.index].interviewed = !item.interviewed;


                         }}

                     ></Task2ListComponent>
                    )
                })
}
                </ul>

            </div>

        )
    }

}

export default Task2;


