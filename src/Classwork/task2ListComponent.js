import React from 'react';
import ButtonClasswork from "./ButtonClasswork";
import Button from "../Childs/Button";

const Task2ListComponent = ({user,action}) =>{
    console.log('action from tasklistCOmponent',action );
    let {name,gender,age,interviewed,key} = user;
     const interviewedChanger = (action) => {
        const {interviewed} = user;

        console.log(user);

        user.interviewed = !user.interviewed;
        console.log(user.interviewed);

         const newStateInterviewed = user.interviewed;
         console.log(user);

        return user;
    }



    return(
        <div>

            <p>{key}</p>
            <p style={user.interviewed ? {backgroundColor:"#ffcc00"} : {}}>{name}</p>
            <p>{gender}</p>
            <p>{age}</p>
            <p>{user.interviewed.toLocaleString()}<span> <ButtonClasswork
                style={{backgroundColor:"#ffcc00",padding:"5px 20px"}}
                action={action}
            >{user.interviewed ? "Got" : "Get"} Interview</ButtonClasswork></span></p>
        </div>

    );
}

Task2ListComponent.defaultProps = {
    user:{
        name:"Have you ever saw default name",
        gender:"MALE",
        age:30
    },interviewed:false
}

export default Task2ListComponent;