import React from 'react';


const ButtonClasswork = ({action,style,children}) => {

const defualtAction = () =>{
    console.log('Log of default props');

    }
    return(
        <button
            onClick={action}
            style={style}
        >{children}</button>
    )
}
ButtonClasswork.defaultProps= {
    action:() => {
        console.log('Log of default props');
    },
    style:{backgroundColor:"#bbff00",padding:"5px 20px"},
    children:"default mes"

}

export default ButtonClasswork;