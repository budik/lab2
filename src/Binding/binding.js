import React,{Component} from 'react';




class BindingDemo extends Component{


    constructor(props) {
        super(props);
        this.someStuff = this.someStuff.bind(this);
    }


    someStuff(){
        console.log('Somestuff',this);
    }

    someStuffArrow = () => {
        console.log('someStuffArrow',this);
    }

    render() {
        const {someStuff,someStuffArrow} = this;
        return (
            <div>
                <h1>Binding </h1>
                {/*andtipattern 1*/}
                <button onClick={someStuff.bind(this)}>Anti1</button>
                {/*andtipattern 2*/}
                <button onClick={(e) =>{this.someStuff()}}>Anti2</button>

                {/*PAttern 1*/}
                <button onClick={someStuff}>pattern1</button>
                {/*PAttern 2*/}
                <button onClick={someStuffArrow}>pattern2</button>

            </div>
        )
    }

}


export default BindingDemo;
